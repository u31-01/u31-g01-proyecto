

// Importar mongoose


const mongoose = require('mongoose');


// establecer el schema del doc en la collection

const proveedorSchema = mongoose.Schema({

    id_prov : Number,
    nombre_prov : String,
    direccion_prov : String,
    ciudad_prov : String,
    telefono_prov : Number
    
    

},
{
    versionkey : false
}

);

// exportar para utlizar en otros Scripts

module.exports = mongoose.model('proveedores', proveedorSchema);

//usarlo en el controlador => CRUD 