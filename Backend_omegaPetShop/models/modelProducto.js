
// Importar mongoose


const mongoose = require('mongoose');


// establecer el schema del doc en la collection

const productoSchema = mongoose.Schema({

    id_prod : Number,
    nombre_prod : String,
    cant_stock_prod : Number,
    id_categoria : [mongoose.Types.ObjectId],
    precio_prod : Number,
    id_proveedor : [mongoose.Types.ObjectId]
},
{
    versionkey : false
});

// exportar para utlizar en otros Scripts

module.exports = mongoose.model('productos', productoSchema);

//usarlo en el controlador => CRUD 