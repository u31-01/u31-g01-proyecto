
// Importar mongoose


const mongoose = require('mongoose');


// establecer el schema del doc en la collection

const categoriaSchema = mongoose.Schema({

    id_cat : Number,
    nombre_cat : String,
    
},
{
    versionkey : false
});

// exportar para utlizar en otros Scripts

module.exports = mongoose.model('categoria', categoriaSchema);

//usarlo en el controlador => CRUD 