
// Importar mongoose


const mongoose = require('mongoose');


// establecer el schema del doc en la collection

const clienteSchema = mongoose.Schema({

    id_cliente : Number,
    nombre_clie : String,
    telefono_clie : Number,
    direccion_clie : String,
    ciudad_clie : String
    
},
{
    versionkey : false
});

// exportar para utlizar en otros Scripts

module.exports = mongoose.model('clientes', clienteSchema);

//usarlo en el controlador => CRUD 