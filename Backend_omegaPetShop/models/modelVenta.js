// Importar mongoose


const mongoose = require('mongoose');


// establecer el schema del doc en la collection

const ventaSchema = mongoose.Schema({

    id_ventas : [mongoose.Types.ObjectId],
    id_cliente: [mongoose.Types.ObjectId],
    id_prod : [mongoose.Types.ObjectId],
    fecha_vent : String,
    cantidad_vent : Number,
    valor_unitario_vent : Number,
    valor_total_vent : Number
    
},
{
    versionkey : false
});

// exportar para utlizar en otros Scripts

module.exports = mongoose.model('ventas', ventaSchema);

//usarlo en el controlador => CRUD 