

// importar mongoose

const mongoose = require('mongoose');

// importar las variables de entorno


require('dotenv').config('../var.env');

// Establece la conexión en una funcion

const conexionDB = async() => {

    try {
        
        await mongoose.connect(process.env.URI_MONGODB);
        console.log("Base de Datos Conectada... ")

    } catch (error) {
        
        console.log("Error:" + error);
        process.exit(1);

    }
}

module.exports = conexionDB;