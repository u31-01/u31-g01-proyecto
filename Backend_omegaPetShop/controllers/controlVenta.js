

const modelVenta = require('../models/modelVenta');



// podemos exportar directamente cada uno de los métodos del CRUD

// CRUD => Create

exports.crear = async(req, res) => {

    try {
        
        let venta;

        // Establecer los datos a guardar
        venta = new modelVenta(req.body);
       

        // });
         await venta.save();

         // respuesta para verificar la variable

         res.send(venta); 

    } catch (error) {
        
        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al guardar el venta ...");

    }

}




// CRUD => Read


exports.obtener = async(req, res) => {

    try {
        
        // consultando la base de datos

        const venta = await modelVenta.find();
        res.json(venta);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al guardar la venta...");
    }
} 

exports.obtenerPorId = async(req, res) => {

    try {
        
        // consultando la base de datos

        const venta = await modelVenta.findById (req.params.id);
        res.json(venta);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al mostrar el venta...");
    }
} 



// CRUD => Update

exports.actualizar = async (req, res) => {

    try {

        const venta = await modelVenta.findById(req.params.id);

        if( !venta){

            res.status(404).json({msg: "El venta no existe"});


        }else{
            await modelVenta.findByIdAndUpdate({_id : req.params.id}, req.body);
           // await modelProveedor.findByIdAndUpdate({_id : req.params.id}, {nombre_prov : "UIS MisionTIC 2022"});
            res.json({mensaje : 'venta actualizado correctamente...'});
        }

    } catch (error) {

        console.log("Error al Guardar Datos" + error);
        res.status(500).send("Error al actualizar el venta");

    }

}


// CRUD  => DELETE

exports.eliminar = async (req, res) => {

    try {

        const venta = await modelVenta.findById(req.params.id);

        if(!venta){

            res.status(404).json({mensaje : 'El venta no exite'})
        }else{

            await modelVenta.findByIdAndRemove({_id : req.params.id});
            res.json({msg: 'venta eliminado correctamente'});

        }
        
    } catch (error) {

        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al eliminar el proveedor...");
        
    }


}