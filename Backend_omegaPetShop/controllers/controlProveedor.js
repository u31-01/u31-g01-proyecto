

// importacion para el modelo de proveedor 

const modelProveedor = require('../models/modelProveedor');

// podemos exportar directamente cada uno de los métodos del CRUD

// CRUD => Create

exports.crear = async(req, res) => {

    try {
        
        let proveedor;

        // Establecer los datos a guardar
        //proveedor = new modelProveedor(req.body);
        proveedor = new modelProveedor({

            id_prov : 13,
            nombre_prov :"UIS",
            direccion_prov : "calle 43a # 65-23",
            ciudad_prov : "Bogota",
            telefono_prov : 3228547896
            

        });
         await proveedor.save();

         // respuesta para verificar la variable

         res.send(proveedor);

    } catch (error) {
        
        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al guardar el proveedor...");

    }

}

// CRUD => Read


exports.obtener = async(req, res) => {

    try {
        
        // consultando la base de datos

        const proveedor = await modelProveedor.find();
        res.json(proveedor);


    } catch (error) {

         console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al guardar el proveedor...");
    }
} 


// CRUD => Update

exports.actualizar = async (req, res) => {

    try {

        const proveedor = await modelProveedor.findById(req.params.id);

        if( !proveedor){

            res.status(404).json({msg: "El proveedor no existe"});


        }else{
            //await modelProveedor.findByIdAndUpdate({_id : req.params.id}, req.body);
            await modelProveedor.findByIdAndUpdate({_id : req.params.id}, {nombre_prov : "UIS MisionTIC 2022"});
            res.json({mensaje : 'proveedor actualizado correctamente'});
        }




    } catch (error) {

        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al actualizar el proveedor");

    }

}


// CRUD  => DELETE

exports.eliminar = async (req, res) => {

    try {

        const proveedor = await modelProveedor.findById(req.params.id);

        if(!proveedor){

            res.status(404).json({mensaje : 'El proveedor no exite'})
        }else{

            await modelProveedor.findByIdAndRemove({_id : req.params.id});
            Response.json({msg: 'proveedor eliminado correctamente'});

        }
        
    } catch (error) {

        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al eliminar el proveedor...");
        
    }


}