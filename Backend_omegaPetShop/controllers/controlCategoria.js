

const modelCategoria = require('../models/modelCategoria');



// podemos exportar directamente cada uno de los métodos del CRUD

// CRUD => Create

exports.crear = async(req, res) => {

    try {
        
        let categoria;

        // Establecer los datos a guardar
        categoria = new modelCategoria(req.body);
       

        // });
         await categoria.save();

         // respuesta para verificar la variable

         res.send(categoria); 

    } catch (error) {
        
        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al guardar el categoria ...");

    }

}




// CRUD => Read


exports.obtener = async(req, res) => {

    try {
        
        // consultando la base de datos

        const categoria = await modelCategoria.find();
        res.json(categoria);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al guardar el categoria...");
    }
} 

exports.obtenerPorId = async(req, res) => {

    try {
        
        // consultando la base de datos

        const categoria = await modelCategoria.findById (req.params.id);
        res.json(categoria);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al mostrar el categoria...");
    }
} 



// CRUD => Update

exports.actualizar = async (req, res) => {

    try {

        const categoria = await modelCategoria.findById(req.params.id);

        if( !categoria){

            res.status(404).json({msg: "El categoria no existe"});


        }else{
            await modelCategoria.findByIdAndUpdate({_id : req.params.id}, req.body);
           // await modelProveedor.findByIdAndUpdate({_id : req.params.id}, {nombre_prov : "UIS MisionTIC 2022"});
            res.json({mensaje : 'categoria actualizado correctamente...'});
        }

    } catch (error) {

        console.log("Error al Guardar Datos" + error);
        res.status(500).send("Error al actualizar el categoria");

    }

}


// CRUD  => DELETE

exports.eliminar = async (req, res) => {

    try {

        const categoria = await modelCategoria.findById(req.params.id);

        if(!categoria){

            res.status(404).json({mensaje : 'El categoria no exite'})
        }else{

            await modelCategoria.findByIdAndRemove({_id : req.params.id});
            res.json({msg: 'categoria eliminado correctamente'});

        }
        
    } catch (error) {

        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al eliminar el proveedor...");
        
    }


}