

const modelProducto = require('../models/modelProducto');



// podemos exportar directamente cada uno de los métodos del CRUD

// CRUD => Create

exports.crear = async(req, res) => {

    try {
        
        let producto;

        // Establecer los datos a guardar
        producto = new modelProducto(req.body);
       

        // });
         await producto.save();

         // respuesta para verificar la variable

         res.send(producto); 

    } catch (error) {
        
        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al guardar el producto ...");

    }

}




// CRUD => Read


exports.obtener = async(req, res) => {

    try {
        
        // consultando la base de datos

        const producto = await modelProducto.find();
        res.json(producto);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al guardar el producto...");
    }
} 

exports.obtenerPorId = async(req, res) => {

    try {
        
        // consultando la base de datos

        const producto = await modelProducto.findById (req.params.id);
        res.json(producto);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al mostrar el producto...");
    }
} 



// CRUD => Update

exports.actualizar = async (req, res) => {

    try {

        const producto = await modelProducto.findById(req.params.id);

        if( !producto){

            res.status(404).json({msg: "El producto no existe"});


        }else{
            await modelProducto.findByIdAndUpdate({_id : req.params.id}, req.body);
           // await modelProveedor.findByIdAndUpdate({_id : req.params.id}, {nombre_prov : "UIS MisionTIC 2022"});
            res.json({mensaje : 'producto actualizado correctamente...'});
        }

    } catch (error) {

        console.log("Error al Guardar Datos" + error);
        res.status(500).send("Error al actualizar el producto");

    }

}


// CRUD  => DELETE

exports.eliminar = async (req, res) => {

    try {

        const producto = await modelProducto.findById(req.params.id);

        if(!producto){

            res.status(404).json({mensaje : 'El producto no exite'})
        }else{

            await modelProducto.findByIdAndRemove({_id : req.params.id});
            res.json({msg: 'producto eliminado correctamente'});

        }
        
    } catch (error) {

        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al eliminar el proveedor...");
        
    }


}