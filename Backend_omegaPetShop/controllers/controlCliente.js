

const modelCliente = require('../models/modelCliente');



// podemos exportar directamente cada uno de los métodos del CRUD

// CRUD => Create

exports.crear = async(req, res) => {

    try {
        
        let cliente;

        // Establecer los datos a guardar
        cliente = new modelCliente(req.body);
       

        // });
         await cliente.save();

         // respuesta para verificar la variable

         res.send(cliente); 

    } catch (error) {
        
        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al guardar el cliente ...");

    }

}




// CRUD => Read


exports.obtener = async(req, res) => {

    try {
        
        // consultando la base de datos

        const cliente = await modelCliente.find();
        res.json(cliente);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al guardar el cliente...");
    }
} 

exports.obtenerPorId = async(req, res) => {

    try {
        
        // consultando la base de datos

        const cliente = await modelCliente.findById (req.params.id);
        res.json(cliente);


    } catch (error) {

         console.log("Error leyendo dato" + error);
        res.status(500).send("Error al mostrar el cliente...");
    }
} 



// CRUD => Update

exports.actualizar = async (req, res) => {

    try {

        const cliente = await modelCliente.findById(req.params.id);

        if( !cliente){

            res.status(404).json({msg: "El cliente no existe"});


        }else{
            await modelCliente.findByIdAndUpdate({_id : req.params.id}, req.body);
           // await modelProveedor.findByIdAndUpdate({_id : req.params.id}, {nombre_prov : "UIS MisionTIC 2022"});
            res.json({mensaje : 'cliente actualizado correctamente...'});
        }

    } catch (error) {

        console.log("Error al Guardar Datos" + error);
        res.status(500).send("Error al actualizar el cliente");

    }

}


// CRUD  => DELETE

exports.eliminar = async (req, res) => {

    try {

        const cliente = await modelCliente.findById(req.params.id);

        if(!cliente){

            res.status(404).json({mensaje : 'El cliente no exite'})
        }else{

            await modelCliente.findByIdAndRemove({_id : req.params.id});
            res.json({msg: 'cliente eliminado correctamente'});

        }
        
    } catch (error) {

        console.log("Error al Guardas Datos" + error);
        res.status(500).send("Error al eliminar el proveedor...");
        
    }


}