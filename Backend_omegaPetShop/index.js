// importar las variables de entorno

const express = require('express');



const moongoose = require('mongoose');


require('dotenv').config({path: 'var.env'});






const router = express.Router();


//Importar la ruta de la configuracion con la base de datos

const conectarDB = require('./config/cxn_db');


const cors = require('cors');
//const Services = require('./src/services');

  


var app = express();

app.use(express.json());

conectarDB();


//app.use(express.json())



// app.get('/', (req, res) =>{
//     res.json({
//         message:"lista de usuarios",
//         body: Services.getUsers(),
//     });
// });


// app.post('/', (req, res) =>{
//     let newUser = req.body;
//     Services.createuser(newUser);
//     res.status(201).json({
//         message: 'Usuario creado',
//         body: Services.createuser(newUser),
//     })
// })  

// app.use('/', function(req, res){

//     res.send("Hola Grupo...")
    
// });

//funcion flecha

// app.use('/', (req, res) => {

//     res.send("Funcion flecha")
// });


//Metodos usados GET POST PUSH DELETE

//const router = express.Router(); AGREGADA ANTES DE INICIALIZAR EXPRESS

app.use(router);

// importar el controlador de proveedores en relacion al CRUD

const controlProveedor = require('./controllers/controlProveedor');


router.post('/apirest/proveedor/', controlProveedor.crear); // Create
router.get('/apirest/proveedor/', controlProveedor.obtener); // Read
router.put('/apirest/proveedor/:id', controlProveedor.actualizar); //Update
router.delete('/apirest/proveedor/:id', controlProveedor.eliminar); // Delete


// importar el controlador de producto en relacion al CRUD

const controlProducto = require('./controllers/controlProducto');


router.post('/apirest/producto/', controlProducto.crear); // Create
router.get('/apirest/producto/', controlProducto.obtener); // Read
router.get('/apirest/producto/:id', controlProducto.obtenerPorId); // Read
router.put('/apirest/producto/:id', controlProducto.actualizar); //Update
router.delete('/apirest/producto/:id', controlProducto.eliminar); // Delete


// importar el controlador de categoria en relacion al CRUD

const controlCategoria= require('./controllers/controlCategoria');


router.post('/apirest/categoria/', controlCategoria.crear); // Create
router.get('/apirest/categoria/', controlCategoria.obtener); // Read
router.get('/apirest/categoria/:id', controlCategoria.obtenerPorId); // Read
router.put('/apirest/categoria/:id', controlCategoria.actualizar); //Update
router.delete('/apirest/categoria/:id', controlCategoria.eliminar); // Delete

// importar el controlador de cliente en relacion al CRUD

const controlCliente = require('./controllers/controlCliente');


router.post('/apirest/cliente/', controlCliente.crear); // Create
router.get('/apirest/cliente/', controlCliente.obtener); // Read
router.get('/apirest/cliente/:id', controlCliente.obtenerPorId); // Read
router.put('/apirest/cliente/:id', controlCliente.actualizar); //Update
router.delete('/apirest/cliente/:id', controlCliente.eliminar); // Delete

// importar el controlador de ventas en relacion al CRUD

const controlVenta = require('./controllers/controlVenta');


router.post('/apirest/venta/', controlVenta.crear); // Create
router.get('/apirest/venta/', controlVenta.obtener); // Read
router.get('/apirest/venta/:id', controlVenta.obtenerPorId); // Read
router.put('/apirest/venta/:id', controlVenta.actualizar); //Update
router.delete('/apirest/venta/:id', controlVenta.eliminar); // Delete




// router.get('/Mensaje', function(req, res) {

//     res.send('Mensaje con el Método GET');

//     // const name_db = 'omegaPetShop';
//     // const user = 'GrupoU31-G01';
//     // const psw = 'GrupoU31-G01';
//     // const uri = `mongodb+srv://GrupoU31-G01:GrupoU31-G01@omegapetshop.pjlftth.mongodb.net/omegaPetShop?retryWrites=true&w=majority`;

//     // moongoose.connect(uri)
//     // moongoose.connect(process.env.URI_MONGODB)
//     //     .then(function(){console.log("Base de datos conectada")})
//     //     .catch(function(e){console.log("Error: " + e)})

//     // conectarDB();
// });

// router.post('/Mensaje', function(req, res){

//     res.send('Mensaje con el Método POST');
// });

// router.put('/Mensaje', function(req, res){

//     res.send('Mensaje con el Método PUT');

// })

// router.delete('/Mensaje', function(req, res){

//     res.send('Mensaje con el Método DELETE');

// })
//-----------------------------------

// app.listen(4000);

//Servidor web activo


app.listen(process.env.PORT);

console.log('Servidor ejecutandose en localhost:4000')
console.log('ctrl c para terminar el servidor')

